<?php

require_once __DIR__ . '/../vendor/autoload.php';

use factoryFramework\RoutingHandler\Request;
use factoryFramework\RoutingHandler\Router;

$request = new Request();

$router = Router::getInstance();

require_once '../routes.php';

$router->resolveRequest($request)->send();