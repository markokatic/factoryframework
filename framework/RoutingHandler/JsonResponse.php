<?php
namespace factoryFramework\RoutingHandler;

class JsonResponse
{
    private $data;
    private $statusCode;
    private $headers;

    public function setStatusCode(int $statusCode): void
    {
        $this->statusCode = $statusCode;
    }

    public function setHeader(string $name, string $value): void
    {
        $this->headers[$name] = $value;
    }

    public function setData($data): void
    {
        $this->data = $data;
    }

    public function send(): void
    {
        http_response_code($this->statusCode);

        foreach ($this->headers as $header) {
            header($header);
        }

        echo json_encode($this->data);
    }
}