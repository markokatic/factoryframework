<?php

namespace factoryFramework\RoutingHandler;

interface ResponseInterface
{
    public function setStatusCode(int $statusCode): void;
    public function setHeader(string $name, string $value): void;
    public function setBody(string $body): void;
    public function send(): void;
}