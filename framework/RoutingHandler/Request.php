<?php

namespace factoryFramework\RoutingHandler;

class Request implements RequestInterface
{
    protected $getParams = [];
    protected $postParams = [];
    protected $serverParams = [];

    public function __construct()
    {
        $this->getParams = $_GET;
        $this->postParams = $_POST;
        $this->serverParams = $_SERVER;
    }

    public function getGetParams($key = null): array|string
    {
        if ($key === null) {
            return $this->getParams;
        }

        return isset($this->getParams[$key]) ? $this->getParams[$key] : null;
    }

    public function getPostParams($key = null): array|string
    {
        if ($key === null) {
            return $this->postParams;
        }
        return isset($this->postParams[$key]) ? $this->postParams[$key] : null;
    }

    public function getMethod(): string
    {
        return $this->server('REQUEST_METHOD');
    }

    public function getRequestParameters(): array
    {
        return $this->getMethod() == 'GET' ? $this->getGetParams() : $this->getPostParams();
    }

    public function server($key = null): array|string
    {
        if ($key === null) {
            return $this->serverParams;
        }
        return isset($this->serverParams[$key]) ? $this->serverParams[$key] : null;
    }

    public function getUrl(): string
    {
        $fullURL = $this->server('REQUEST_URI');
        return $this->parseUrl($fullURL);
    }

    private function parseUrl($fullURL): string
    {
        if ($fullURL == '/') {
            return '/';
        }
        $pos = strpos($fullURL, '?');
        if ($pos) {
            return substr($fullURL, 0, $pos);
        }
        return $fullURL;
    }
}