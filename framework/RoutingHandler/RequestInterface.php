<?php

namespace factoryFramework\RoutingHandler;

interface RequestInterface
{
    public function getGetParams($key = null): array|string;
    public function getPostParams($key = null): array|string;
    public function getMethod(): string;
    public function getRequestParameters(): array;
    public function server($key = null): array|string;
}