<?php

namespace factoryFramework\RoutingHandler;

class Router
{
    private $routes = [];

    private static $instance = null;

    public static function getInstance(): Router
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function addRoute($route): void
    {
        $routeUrl = $route->getUrl();
        if (str_contains($routeUrl, "{") && str_contains($routeUrl, "}")) {
            array_push($this->routes, $route);
        } else {
            array_unshift($this->routes, $route);
        }
    }

    public function resolveRequest($request): Response|JsonResponse
    {
        foreach ($this->routes as $route) {
            if ($route->getMethod() === $request->getMethod() && $this->isCorrectUrl($route->getUrl(), $request->getUrl())) {
                $callback = $route->getCallback();
                return call_user_func_array($callback, $this->getUrlValues($route->getUrl(), $request->getUrl()));
            }
        }
        return $this->routeNotFound();
    }

    private function isCorrectUrl($routeUrl, $requestUrl): bool
    {
        $routeUrl = explode("/", $routeUrl);
        $requestUrl = explode("/", $requestUrl);
        if (sizeof($routeUrl) != sizeof($requestUrl)) {
            return false;
        }
        for ($i = 0; $i < sizeof($routeUrl); $i++) {
            if ($routeUrl[$i] != $requestUrl[$i]) {
                if (!(str_contains($routeUrl[$i], "{") && str_contains($routeUrl[$i], "}"))) {
                    return false;
                }
            }
        }
        return true;
    }

    private function getUrlValues($routeUrl, $requestUrl): array
    {
        $urlValues = [];
        $routeUrl = explode("/", $routeUrl);
        $requestUrl = explode("/", $requestUrl);
        for ($i = 0; $i < sizeof($routeUrl); $i++) {
            if (str_contains($routeUrl[$i], "{") && str_contains($routeUrl[$i], "}")) {
                array_push($urlValues, $requestUrl[$i]);
            }
        }
        return $urlValues;
    }

    private function routeNotFound(): Response
    {
        $response = new Response();
        $response->setBody("Route not found");
        $response->setStatusCode(200);
        return $response;
    }
}