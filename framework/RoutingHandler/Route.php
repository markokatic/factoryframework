<?php

namespace factoryFramework\RoutingHandler;

class Route
{
    private $url;
    private $method;
    private $callback;

    public function __construct(string $url, string $method, callable $callback)
    {
        $this->url = $url;
        $this->method = $method;
        $this->callback = $callback;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function getCallback(): callable
    {
        return $this->callback;
    }
}