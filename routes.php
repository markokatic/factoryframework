<?php

use factoryFramework\RoutingHandler\Route;
use App\Controller\TestController;
use App\Controller\IndexController;

$router->addRoute(new Route('/home', 'GET', function () {
    echo "Home page";
}));

$router->addRoute(new Route('/testpage', 'GET', [new TestController(), 'start']));

$router->addRoute(new Route('/testpage/{id}', 'GET', [new TestController(), 'startPlaceholderTest']));

$router->addRoute(new Route('/index', 'GET', [new IndexController(), 'indexAction']));

$router->addRoute(new Route('/indexJson', 'GET', [new IndexController(), 'indexJsonAction']));