<?php

namespace App\Controller;

use factoryFramework\RoutingHandler\Response;
use factoryFramework\RoutingHandler\JsonResponse;
use Twig\Loader\FilesystemLoader;
use Twig\Environment;

class IndexController
{
    public function indexAction(): Response
    {
        $loader = new FilesystemLoader('./../app/views');
        $twig = new Environment($loader);
        $response = new Response();
        $response->setBody($twig->render('indexAction.html', ["methodName" => "indexAction"]));
        $response->setStatusCode(200);
        return $response;

    }

    public function indexJsonAction(): JsonResponse
    {
        $jsonResponse = new JsonResponse();
        $jsonResponse->setStatusCode(200);
        $jsonResponse->setData(["method" => "indexJsonAction"]);
        return $jsonResponse;
    }
}