<?php

namespace App\Controller;

use factoryFramework\RoutingHandler\Response;

class TestController
{

    public function start(): Response
    {
        $response = new Response();
        $response->setBody("TestController");
        $response->setStatusCode(200);
        return $response;
    }

    public function startPlaceholderTest($id): Response
    {
        $response = new Response();
        $response->setBody("TestController placeholder test:" . $id);
        $response->setStatusCode(200);
        return $response;
    }
}